#!/bin/bash

PACKAGE_NAME=$1
PACKAGE_DESC=$2

bash ./package.json.example > package.json $PACKAGE_NAME "$PACKAGE_DESC"

echo "# $PACKAGE_NAME" > README.md
echo "$PACKAGE_DESC" >> README.md

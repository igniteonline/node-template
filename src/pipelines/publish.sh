#!/bin/bash

AUTH=$1

cat <<EOF > .npmrc
registry=https://npm.ignitedevelopment.com.au/
@igniteonline:registry=https://npm.ignitedevelopment.com.au/
echo //npm.ignitedevelopment.com.au/:_authToken="$AUTH"
EOF
